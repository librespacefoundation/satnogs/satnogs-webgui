<?php

require_once 'includes/nm_functions.php';

function DisplayClientConfigNM() 
{
    // Inintialize wifi devices if uninitialized
    if(!isset($_SESSION['wifi_client_interface'])) {
        getWifiInterfaceNM();
    }

    $clientInterface = $_SESSION['wifi_client_interface'];
    $ifaceStatus = getInterfaceState($clientInterface);
    $ifaceStatus = strcmp($ifaceStatus, "connected") ? "down" : "up";

    echo renderTemplate(
        "configure_client_nm", compact(
            "clientInterface",
            "ifaceStatus"
        )
    );
}
