<?php

require_once 'includes/nm_functions.php';

function DisplayNetworkingConfigNM()
{
    $status = new \RaspAP\Messages\StatusMessage;

    $interfaces_arr = array_fill_keys(getConnectedInterfaces(), array());
    $connectivity = checkConnectivity();
    
    foreach ($interfaces_arr as $key => &$interface) {
        $interface['ip_addresses'] = getInterfaceIpAddresses($key);
        $interface['gateway'] = getInterfaceGateway($key);
        $interface['ipv4_routes'] = getInterfaceRoutesIpv4($key);
        $interface['ipv6_routes'] = getInterfaceRoutesIpv6($key);
        $interface['settings'] = getInterfaceSettings($key);
        // If gateway is empty, the interface is the gateway
        if (!$interface['gateway']) {
            $interface['gateway'] = array_map('removeSubnetMask',
                $interface['ip_addresses']);
        }
    }
    unset($interface);

    echo renderTemplate("networking_nm", compact(
        "interfaces_arr",
        "connectivity")
    );
}

/**
 * Removes the subnet mask and the prefix length from ipv4/ipv6 addressess using
 * str tok.
 * 
 * @param string $ip_addr the ip address
 * @return string the ip address without subnet mask or prefix length number
 */
function removeSubnetMask($ip_addr)
{
    return strtok($ip_addr, '/');
}
