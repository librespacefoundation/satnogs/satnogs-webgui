<?php

require_once 'nm_functions.php';
require_once 'config.php';

function DisplayDHCPConfigNM()
{
    if (isset($_SESSION['status_messages'])) {
        $status = unserialize($_SESSION['status_messages']);
        unset($_SESSION['status_messages']);
    } else {
        $status = new \RaspAP\Messages\StatusMessage;
    }
    
    $hotspot = RASPI_NM_HOTSPOT_CONNECTION_NAME;
    $hotspotStatus = checkConnectionIsActive($hotspot);
    $connections = getActiveConnections();

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if (isset($_POST["savedhcpsettingsnm"])) {
            $conn_name = $_POST["connections"];
            // DHCP connection method
            $result = "";
            $error_flag = false;
            $settings = array();
            if (!strcmp($_POST["connection-ip-method"], "auto")) {
                $settings = array(
                    "ipv4.method" => "auto",
                    "ipv4.address" => "",
                    "ipv4.gateway" => "");
            } elseif (!strcmp($_POST["connection-ip-method"], "manual")) {
                $settings = array(
                    "ipv4.method" => "manual",
                    "ipv4.address" => $_POST['StaticIP'],
                    "ipv4.gateway" => $_POST['DefaultGateway']);
            } elseif (!strcmp($_POST["connection-ip-method"], "shared")) {
                $settings = array(
                    "ipv4.method" => "shared",
                    "ipv4.address" => $_POST['StaticIP']);
            } else {
                $status->addMessage("Uknown Error", "danger");
                $error_flag = true;
            }

            if (filter_ipv4_static_fields($settings, $status)) {
                $status->addMessage("Invalid settings", "danger");
                $error_flag = true;
            }
            // Set connection settings with NM
            if (setConnectionSettings($conn_name, $settings)) {
                $status->addMessage("Failed to set new Settings", "danger");
                $error_flag = true;
            }
            // Restart connection to apply changes if no error in settings
            if (!$error_flag) {
                $result = restartConnection($conn_name);
                $error_flag = strpos($result, "failed") && strpos($result,
                    "Error");
                // Add status message for success and failure
                if ($error_flag === true) {
                    $status->addMessage("Restarting connection $conn_name
                    failed. Settings are not applied.", "danger");
                } else {
                    $status->addMessage("Connection $conn_name restarted
                    successfully.", "success");
                }
            }
        }
        $_SESSION['status_messages'] = serialize($status);
        header("Location: ". $_SERVER['REQUEST_URI'], true, 303);
        exit;
    }

    echo renderTemplate(
        "dhcp_nm", compact(
           "status",
           "hotspotStatus",
           "connections",
           "hotspot"
        )
    );
}

/**
 * Filters the ipv4 fields with static method. On wrong ipv4 address error code
 * -1 is returned. On wrong subnet error code -2 is returned. On wrong ipv4
 * gateway error code -3 is returned.
 */
function filter_ipv4_static_fields($array, $status)
{
    // If method is auto no check is needed
    if (!strcmp($array["ipv4.method"], "auto")) {
        return 0;
    }

    // IPv4 address and subnet is used for both manual and shared so check it
    $ipv4_exploded = explode("/", $array["ipv4.address"]);
    if (!filter_var($ipv4_exploded[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
        $status->addMessage("Invalid IPv4 address", "danger");
        return -1;
    }
    if ($ipv4_exploded[1] < 0 || $ipv4_exploded[1] > 32) {
        $status->addMessage("Invalid subnet", 'danger');
        return -2;
    }
    // For manual also check gateway
    if (!strcmp($array["ipv4.method"], "manual")) {
        if (!filter_var($array["ipv4.gateway"],
                FILTER_VALIDATE_IP,
                FILTER_FLAG_IPV4)) {
            $status->addMessage("Invalid IPv4 gateway", "danger");
            return -3;
        }
    }

    return 0;
}

/**
 * Returns the DNS and DHCP logs from Network Manager unit since last boot
 * 
 * @return array the journalctl output
 */
function getNMDnsDhcpJournal()
{
    exec('journalctl -b -u NetworkManager.service -n 300 | grep "dhcp\|dns"',
        $journal);

    return $journal;
}
