      <?php
        $extraFooterScripts = array();
        $page = $_SERVER['PATH_INFO'];
        // handle page actions
        switch ($page) {
        case "/wlan0_info_nm":
            DisplayDashboardNM($extraFooterScripts);
            break;
        case "/satnogs_station":
            DisplayStation($clientId, $stationId, $stationRegistered);
            break;
        case "/dhcp_conf_nm":
            DisplayDHCPConfigNM();
            break;
        case "/wpa_conf_nm":
            DisplayClientConfigNM();
            break;
        case "/network_conf_nm":
            DisplayNetworkingConfigNM();
            break;
        case "/hotspot_conf_nm":
            DisplayHotspotConfigNM();
            break;
        case "/auth_conf":
            DisplayAuthConfig($_SESSION['user_id']);
            break;
        case "/system_info":
            DisplaySystem($extraFooterScripts);
            break;
        case "/about":
            DisplayAbout();
            break;
        default:
            DisplayDashboardNM($extraFooterScripts);
        }
      ?>

