<?php

require_once 'includes/config.php';
require_once 'includes/nm_functions.php';

/**
 * Show dashboard page.
 */
function DisplayDashboardNM(&$extraFooterScripts)
{
    if (isset($_SESSION['status_messages'])) {
        $status = unserialize($_SESSION['status_messages']);
        unset($_SESSION['status_messages']);
    } else {
        $status = new \RaspAP\Messages\StatusMessage;
    }

    //Check if exec is available
    if (!function_exists('exec')) {
        $status->addMessage(_('Required exec function is disabled. Check if exec is not added to php disable_functions.'), 'danger');
        $status->showMessages();
        return;
    }

    // Get ap interface
    if (!isset($_SESSION['ap_interface'])) {
        $apInterface = getWifiInterfaceNM();
    }
    else {
        $apInterface = $_SESSION['ap_interface'];
    }
    // Set up / down for render template depending ap interface state
    $apInterface_state = getInterfaceState($apInterface);
    
    // strcmp returns 0 when equality
    $ifaceStatus = strcmp($apInterface_state, "connected") ? "down" : "up";

    // Get wireless client card info
    // Client interface
    $clientInterface = $_SESSION['wifi_client_interface'];

    // SSID of connected AP
    $connectedSSID = getConnectedSSID($clientInterface);
    $connectedSSID = $connectedSSID ? $connectedSSID : "-";
    // BSSID of connected AP
    $connectedBSSID = getConnectedBSSID($clientInterface);
    $connectedBSSID = $connectedBSSID ? $connectedBSSID : "-";
    // Bit rate of connected AP
    $bitrate = getConnectedApBitRate($clientInterface);
    $bitrate = strcmp($bitrate, "unknown") ? $bitrate . ' Mbits/s' : "-";
    // RSSI of connected AP
    $signalLevel = getConnectedApRssi($clientInterface);
    $signalLevel = $signalLevel ? $signalLevel . ' dBm' : "-";
    // Active connection name
    $activeConnection = getActiveConnection($clientInterface);
    $txPower = getConnectionTxPower($activeConnection);
    // Tx Power of interface
    $txPower = $txPower ? $txPower . ' dBm' : "Automatic";
    // Frequency of connected AP
    $frequency = getConnectedApFreq($clientInterface);
    $frequency = $frequency ? $frequency . ' MHz' : "-";
    // Link quality
    $strLinkQuality = getConnectedApSignal($clientInterface);
    $strLinkQuality = $strLinkQuality ? $strLinkQuality : 0;

    // Start/Stop Wifi Client
    $clientInterfaceStatus = getInterfaceState($clientInterface);
    // The name makes no sense because there is a something wrong in the
    // original RaspAP. We keep the same var names for compatibility with 
    // existing dashboard template.
    $wlan0up = strcmp($clientInterfaceStatus, "connected") ? false : true;
    
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST['ifdown_wlan0'])) {
            // Pressed Stop button
            if ($wlan0up) {
                $status->addMessage(sprintf(_('Interface is going %s.'), _('down')), 'warning');
                disconnectInterface($clientInterface);
                $status->addMessage(sprintf(_('Interface is now %s.'), _('down')), 'success');
            }
        }
        elseif (isset($_POST['ifup_wlan0'])) {
            // Pressed start button
            if (!$wlan0up) {
                $status->addMessage(sprintf(_('Interface is going %s.'), _('up')), 'warning');
                connectInerface($clientInterface);
                $status->addMessage(sprintf(_('Interface is now %s.'), _('up')), 'success');
            }
        }
        else {
            $status->addMessage(sprintf(_('Interface is %s.'), $clientInterfaceStatus), 'warning');
        }
        $_SESSION['status_messages'] = serialize($status);
        header("Location: ". $_SERVER['REQUEST_URI'], true, 303);
        exit;
    }

    $clientInterfaceStatus = getInterfaceState($clientInterface);
    $wlan0up = strcmp($clientInterfaceStatus, "connected") ? false : true;
    $apInterface_state = getInterfaceState($apInterface);
    $ifaceStatus = strcmp($apInterface_state, "connected") ? "down" : "up";
    
    echo renderTemplate(
        "dashboard_nm", compact(
            "status",
            "apInterface",
            "ifaceStatus",
            "clientInterface",
            "connectedSSID",
            "connectedBSSID",
            "bitrate",
            "signalLevel",
            "txPower",
            "frequency",
            "strLinkQuality",
            "wlan0up"
        )
    );
    
    $extraFooterScripts[] = array('src'=>'app/js/dashboardchart_nm.js', 'defer'=>false);
    $extraFooterScripts[] = array('src'=>'app/js/linkquality.js', 'defer'=>false);
}
