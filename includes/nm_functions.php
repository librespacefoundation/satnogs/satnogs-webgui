<?php
/**
 * Returns if Network Manager is running
 * @return boolean running
 */
function networkManagerRunning()
{
    exec("nmcli -t -f running general", $running);

    return $running[0] ? True : False;
}

/**
 * Checks available wifi interfaces with nmcli and designates one for the 
 * hotspot. If an interface exists matching the name from config file assign
 * that for the hotspot, else one at random. If multiple interfaces exist also
 * assign one as the client interface.
 * 
 * @return string the name of the interface designated for the hotspot
 */
function getWifiInterfaceNM()
{
    exec("nmcli device status | awk '$2==\"wifi\" {print $1}'",
        $interface_list);

    // If an interface with name as in the configuration exists set it as the
    // interface for creating the hotspot
    if ($key = array_search(RASPI_WIFI_AP_INTERFACE, $interface_list)) {
        $interface = $_SESSION['ap_interface'] = RASPI_WIFI_AP_INTERFACE;
        unset($interface_list[$key]);
        $interface_list = array_values($interface_list);
    }
    else {
        $interface = $_SESSION['ap_interface'] = $interface_list[0];
        unset($interface_list[0]);
        $interface_list = array_values($interface_list);
    }

    // if more than 1 wifi interfaces available set second as client
    $client_interface = $_SESSION['wifi_client_interface'] = 
        !empty($interface_list) ? $interface_list[0] : $interface;

    // specifically for rpi0W in AP-STA mode, the above check ends up with the 
    // interfaces crossed over (wifi_client_interface vs 'ap_interface'), 
    // because the second interface (uap0) is created by raspap and used as the
    // access point.
    if ($client_interface == "uap0"  && ($arrHostapdConf['WifiAPEnable'] ?? 0)){
        $_SESSION['wifi_client_interface'] = $interface;
        $_SESSION['ap_interface'] = $client_interface; 
    }

    return $_SESSION['ap_interface'];
}

/**
 * Return array of networks seen by interface. Each entry is an array with
 * keys active, ssid, chan, rate, signal (%), bssid, rssi. If empty returns
 * empty array.
 * 
 * @param string $interface name of interface
 * @return array the array of networks arrays
 */
function getAvailableWifiNetworks($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -t -f active,ssid,freq,chan,rate,signal,security,bssid device\
          wifi list --rescan yes ifname $interface", $networks);
    
    // If array is empty return
    if (!$networks) {
        return $networks;
    }

    $keys = array("active", "ssid", "freq", "channel", "rate", "signal",
                  "security", "bssid");
    foreach ($networks as &$net) {
        $net = explode(":", $net, count($keys));
        $net = array_combine($keys, array_values($net));
        $net["active"] = strcmp("yes", $net["active"]) ? false : true;
        $net["bssid"] = stripslashes($net["bssid"]);
        $net["rssi"] = round(($net["signal"] / 100) * (60) -100);
        $networks_arr[$net["ssid"]] = $net;
    }

    return $networks_arr;
}

/**
 * Return array of known connections. Each entry is an array with keys ssid,
 * security, password. If empty return empty array.
 * 
 * @return array the array of networks arrays
 */
function getKnownWifiNetworks()
{
    exec("nmcli -t -f name,type connection show |
          awk -F \":\" '$2==\"802-11-wireless\" {print $1}'",
          $wifi_connections);
    
    $fields = "802-11-wireless.ssid,".
               "802-11-wireless-security.key-mgmt,".
               "802-11-wireless-security.psk";
    $keys = ["ssid", "security", "password"];
    foreach ($wifi_connections as $conn) {
        $arg = escapeshellarg($conn);
        exec("nmcli -g $fields connection show $arg --show-secrets 2>&1", $out);

        $out = array_combine($keys, array_values($out));
        $networks[$out["ssid"]] = $out;
        $networks[$out["ssid"]]["configured"] = true;
        unset($out);
    }

    return $networks;
}

/**
 * Return the mac address of the specified interface
 *
 * @param string $interface name of the interface
 * @return string the mac address of the interface
 */
function getMacAddressNM($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -e no -m multiline -f general.hwaddr device show $interface |
            awk '{print $2}'", $mac_addr);

    return $mac_addr;
}

/**
 * Return array with ipv4 addresses of the specified interface
 * 
 * @param string $interace name of the interface
 * @return array the ipv4 address of the interface
 */
function getIp4AddressesNM($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -e no -m multiline -f ip4.address device show $interface |
            awk '{print $2}' | cut -d'/' -f1", $ip4_addresses);

    return $ip4_addresses;
}

/**
 * Return array with ipv6 addresses of the specified interface
 * 
 * @param string $interace name of the interface
 * @return array the ipv6 address of the interface
 */
function getIp6AddressesNM($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -e no -m multiline -f ip6.address device show $interface |
            awk '{print $2}' | cut -d'/' -f1", $ip6_addresses);

    return $ip6_addresses;
}

/**
 * Return array with subnet masks of the specified interface
 * 
 * @param string $interface name of the interface
 * @return array the subnet mask of the interface
 */
function getSubetMaskNM($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -e no -m multiline -f ip4.address device show $interface |
            awk '{print $2}' | awk -F'/' '{print $2}'", $subnet);
    
    $all_ones = 2**32 - 1;
    foreach ($subnet as $val) {
        $tmp = 2**(32 - $val) - 1;
        $subnet_mask[] = long2ip($all_ones - $tmp);
    }

    return $subnet_mask;
}

/**
 * Returns the ssid if interface is connected to AP
 * 
 * @param string $interface name of the interface
 * @return string the ssid or null
 */
function getConnectedSSID($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f active,ssid device wifi list ifname $interface --rescan no |
            awk '$1==\"yes\" {print $2}'", $ssid);

    return $ssid ? $ssid[0] : null ;
}

/**
 * Returns the bssid if interface is connected to AP
 * 
 * @param string $interface name of the interface
 * @return string the bssid
 */
function getConnectedBSSID($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f active,bssid device wifi list ifname $interface --rescan no |
            awk '$1==\"yes\" {print $2}'", $bssid);

    return $bssid ? $bssid[0] : null;
}

/**
 * Returns the frequency if interface is connected to AP
 * 
 * @param string $interface name of the interface
 * @return string the frequency
 */
function getConnectedApFreq($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f active,freq device wifi list ifname $interface --rescan no |
            awk '$1==\"yes\" {print $2}'", $freq);

    return $freq ? $freq[0] : null;
}

/**
 * Returns the rssi if interface is connected to AP
 * 
 * @param string $interface name of the interface
 * @return int the rssi
 */
function getConnectedApRssi($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f active,signal device wifi list ifname $interface --rescan no |
            awk '$1==\"yes\" {print $2}'", $signal_perc);

    if (!$signal_perc) {
        return null;
    }
    // Calculate value in dBm from percentage
    // new_value = ((old_value - old_min) / (old_max - old_min)) * (new_max - new_min) + new_min
    $rssi = round(($signal_perc[0] / 100) * (60) -100);

    return (int)$rssi;
}

/**
 * Returns signal quality as percentage if interface is connected to AP
 * 
 * @param string $interface name of the interface
 * @return int the percentage of signal quality
 */
function getConnectedApSignal($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f active,signal device wifi list ifname $interface --rescan no |
            awk '$1==\"yes\" {print $2}'", $signal_perc);

    return $signal_perc ? $signal_perc[0] : null;
}

/**
 * Returns the tx bitrate if interface is connected to AP
 * 
 * @param string $interface name of the interface
 * @return int the tx bitrate
 */
function getConnectedApBitRate($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f capabilities.speed device show $interface |
            awk '{print $2}'", $rate);

    return $rate[0];
}

/**
 * Returns the name of active connection for the interface if connected else 
 * empty string.
 * 
 * @param string $interface name of the interface
 * @return string the active connection name
 */
function getActiveConnection($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -g general.connection device show $interface", $connection);

    return strcmp($connection[0], "--") ? $connection[0] : "";
}

/**
 * Returns an array with all active connections names.
 * 
 * @return array $connections the array with active connections names
 */
function getActiveConnections()
{
    exec("nmcli -t -f name connection show --active", $connections);

    return $connections;
}

/**
 * Returns a sting with the ipv4.method of the connection.
 * 
 * @param string $conn_name the connection name
 * @return string $ipv4_method the ipv4 method of the connection
 */
function getConnectionIPv4Method($conn_name)
{
    $conn_name = escapeshellarg($conn_name);
    exec("nmcli -t -g ipv4.method connection show $conn_name", $ipv4_method);

    return $ipv4_method[0];
}

/**
 * Returns an array with the ipv4 settings of the connections with keys the
 * settings and values the values
 * 
 * @param string $conn_name the connection name
 * @return array $ipv4_settings the ipv4 settings
 */
function getConnectionIPv4Settings($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli -t -f ipv4 connection show $conn_name", $ipv4_settings);
    
    foreach ($ipv4_settings as $key => $setting) {
        $tmp = explode(':', $setting);
        $ipv4_settings[$tmp[0]] = $tmp[1];
        unset($ipv4_settings[$key]);
    }

    return $ipv4_settings;
}

/**
 * Returns the tx power for the connection. If the connection is not available
 * to all users the function will return NULL.
 * If the tx power value is not statically set the function will return 0.
 * 
 * @param string $conn_name name of the connection
 * @return int the tx power
 */
function getConnectionTxPower($conn_name)
{
    $conn_name = escapeshellarg($conn_name);
    exec("nmcli -f 802-11-wireless.tx-power connection show $conn_name |
            awk '{print $2}'", $tx_power);
    
    return $tx_power ? $tx_power[0] : null;
}

/**
 * Returns the state of the interface. State can be connected, disconnected or
 * unavailable.
 * 
 * @param string $interface name of the interface
 * @return string the state
 */
function getInterfaceState($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -f device,state device status |
            awk -v interface=$interface '$1==interface {print $2}'",
            $state);

    return $state[0];
}

/**
 * Deactivates the connection of the interface if there is one active and 
 * returns the result. If there is no active connection error occurs. After 
 * deactivation prevents auto activating another connection.
 * 
 * @param string $interface name of the interface
 * @return string command result message
 */
function disconnectInterface($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli device disconnect $interface 2>&1", $disconnected);

    return $disconnected[0];
}

/**
 * Activates a suitable connection on the interface and returns the result. If 
 * no suitable connection is found a new one is cretated.
 * 
 * @param string $interface name of the interface
 * @return string command result message
 */
function connectInerface($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli device connect $interface 2>&1", $connected);

    return $connected;
}

/**
 * Returns bytes received by the interface.
 * 
 * @param string $interface name of the interface
 * @return string bytes received
 */
function getInterfaceRxBytes($interface)
{
    $interface = escapeshellarg($interface);
    $SERVICE = "org.freedesktop.NetworkManager";
    $INTERFACE = $SERVICE.".Device.Statistics";
    $PROPERTY = "RxBytes";

    exec("nmcli -g general.dbus-path device show $interface", $dbus_path);

    exec("busctl get-property $SERVICE $dbus_path[0] $INTERFACE $PROPERTY |
            awk '{print $2}'", $rx_bytes);

    return $rx_bytes[0];
}

/**
 * Returns bytes transmitted by the interface.
 * 
 * @param string $interface name of the interface
 * @return string bytes transmitted
 */
function getInterfaceTxBytes($interface)
{
    $interface = escapeshellarg($interface);
    $SERVICE = "org.freedesktop.NetworkManager";
    $INTERFACE = $SERVICE.".Device.Statistics";
    $PROPERTY = "TxBytes";

    exec("nmcli -g general.dbus-path device show $interface", $dbus_path);

    exec("busctl get-property $SERVICE $dbus_path[0] $INTERFACE $PROPERTY |
            awk '{print $2}'", $tx_bytes);

    return $tx_bytes[0];
}

/**
 * Set the RefreshRateMs property of the 
 * org.freedesktop.NetworkManager.Device.Statistics interface for the interface.
 * 
 * @param string $interface the name of the interface
 * @param int $ms refresh rate in milliseconds
 */
function setInterfaceRefreshStatsRate($interface, $ms = 1000)
{
    $interface = escapeshellarg($interface);
    $SERVICE = "org.freedesktop.NetworkManager";
    $INTERFACE = $SERVICE.".Device.Statistics";
    $PROPERTY = "RefreshRateMs u";

    exec("nmcli -g general.dbus-path device show $interface", $dbus_path);

    exec("busctl set-property $SERVICE $dbus_path[0] $INTERFACE $PROPERTY $ms 2>&1",
         $result);

    return $result;
}

/**
 * Updates the password to a known connection. If the update is successfull an
 * empty string is returned, else the return output of the command.
 * 
 * @param string $conn_name connection name to be updated
 * @param string $password new password
 * @return string nmcli command return value 
 */
function updateConnectionPassword($conn_name, $password)
{
    $conn_name = escapeshellarg($conn_name);
    $password = escapeshellarg($password);

    exec("nmcli connection modify $conn_name 802-11-wireless-security.psk $password 2>&1", $result);
    
    return $result[0];
}

/**
 * Connects to an existing, or creates a new, connection with ssid and password.
 * DHCP IP configuration is assumed.
 * 
 * @param string $ifname name of the interface to connect
 * @param string $ssid ssid of the wifi network
 * @param string $password password of the wifi network
 * @return string $result output of the command
 */
function connectWifi($ifname, $ssid, $password)
{
    $ssid = escapeshellarg($ssid);
    $password = escapeshellarg($password);
    $ifname = escapeshellarg($ifname);

    exec("nmcli device wifi connect $ssid password $password ifname $ifname 2>&1", $result);

    return $result[0];
}

/**
 * Deletes an existing connection.
 * 
 * @param string $conn_name connection name
 * @return string $result output of the command
 */
function deleteConnection($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli connection delete $conn_name 2>&1", $result);

    return $result[0];
}

/**
 * Returns an array with available interfaces with WiFi capabilities.
 * 
 * @return array $result the array with wireless interfaces
 */
function getWirelessInterfaces()
{
    exec("nmcli -t -f device,type device status |
            awk -F':' '$2==\"wifi\" {print $1}'", $result);

    return $result;
}

/**
 * Returns an array with all interfaces names.
 * 
 * @return array $result the array with interfaces names
 */
function getInterfaces()
{
    exec("nmcli -t -f device device status", $result);

    return $result;
}

/**
  * Returns interfaces names for connected interfaces.
  * 
  * @return array $result interfaces names 
  */
function getConnectedInterfaces()
{
    exec("nmcli -t -f device connection show --active", $result);

    return $result;
}

/**
 * Get network connectivity state result. Result can be 
 * none: the host is not connected to any network.
 * portal: the host is behind a captive portal and cannot reach the internet.
 * limited: the host is connected to a network, but it has no access to the Internet.
 * full: the host is connected to a network and has full access to the Internet.
 * none: connectivity status cannot be found.
 * 
 * @return string $result connectivity state
 */
function checkConnectivity()
{
    exec("nmcli networking connectivity check", $result);

    return $result[0];
}

/**
 * Returns an array with ipv4 and ipv6 addresses assigned to an interface.
 * 
 * @param string $interface interface name
 * @return array $result ipv4/ipv6 addresses of interface
 */
function getInterfaceIpAddresses($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -t -g ip4.address,ip6.address device show $interface", $result);

    // Remove shell escaped ':'
    foreach ($result as &$gateway) {
        if ($gateway) {
            $gateway = str_replace('\\', '', $gateway);
        }
    }

    return $result;
}

/**
 * Returns an array with ipv4 and ipv6 gateway addressess assigned to an interface
 * or NULL
 * 
 * @param string $interface interface name
 * @return array $result ipv4/ipv6 gateway addresses of interface
 */
function getInterfaceGateway($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -t -g ip4.gateway,ip6.gateway device show $interface", $result);

    // Remove shell escaped ':'
    foreach ($result as &$gateway) {
        if ($gateway) {
            $gateway = str_replace('\\', '', $gateway);
        }
    }

    return $result[0] ? $result : NULL;
}

/**
 * Returns an array with ipv4 routes for the interface
 * 
 * @param string $interface the interface name
 * @return array $result the array with route rules
 */
function getInterfaceRoutesIpv4($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -t -g ip4.route device show $interface", $result);

    // Create array with routes from return string
    $result = explode(' | ', $result[0]);
    // Remove empty strings
    $result = array_filter($result);
    // Concatenate interface to each string
    foreach ($result as &$route) {
        $route = $route . ', dev ' . $interface;
    }

    return $result;
}

/**
 * Returns an array with ipv6 routes for the interface
 * 
 * @param string $interface the interface name
 * @return array $result the array with route rules
 */
function getInterfaceRoutesIpv6($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -t -g ip6.route device show $interface", $result);

    // Remove shell escaped ':'
    foreach ($result as &$gateway) {
        if ($gateway) {
            $gateway = str_replace('\\', '', $gateway);
        }
    }
    
    // Create array with routes from return string
    $result = explode(' | ', $result[0]);
    // Remove empty strings
    $result = array_filter($result);
    // Concatenate interface to each string
    foreach ($result as &$route) {
        $route = $route . ', dev ' . $interface;
    }

    return $result;
}

/**
 * Returns an array with interface settings
 * 
 * @param string $interface the interface name
 * @return array $result array with settings
 */
function getInterfaceSettings($interface)
{
    $interface = escapeshellarg($interface);
    exec("nmcli -t device show $interface", $result);

    return $result;
}

/**
 * Starts a hotspot and creates a new connection with the name Hotspot.
 * If the connection already exists it only starts the hotspot.
 * 
 * @param string $interaface wifi interface used for hotspot
 * @param string $ssid ssid of the hotspot
 * @param string $password password of the hotspot
 * @param string $channel channel hotspot broadcasts
 * @param string $mobandde hotspot band, can be a or bg
 * @return string $result output of the command
 */
function startCreateHotspot($interface = "wlan0",
                      $ssid = "raspi-webgui",
                      $password = "ChangeMe",
                      $channel = "1",
                      $band = "bg")
{
    $interface = escapeshellarg($interface);
    $ssid = escapeshellarg($ssid);
    $password = escapeshellarg($password);
    $channel = escapeshellarg($channel);
    $band = escapeshellarg($band);

    exec("nmcli device wifi hotspot 
            ifname $interface
            ssid $ssid
            password $password
            channel $channel
            band $band 2>&1",
            $result);

    return $result[0];
}

/**
 * Sets the ssid of a connection.
 * 
 * @param string $conn_name name of the connection
 * @param string $ssid new ssid
 * @return string $result output of the command
 */
function setConnectionSSID($conn_name, $ssid)
{
    $conn_name = escapeshellarg($conn_name);
    $ssid = escapeshellarg($ssid);

    exec("nmcli connection modify $conn_name 802-11-wireless.ssid $ssid",
            $result);

    return $result[0];
}

/**
 * Sets the channel of the connection.
 * 
 * @param string $conn_name name of the connection
 * @param string $channel new channel
 * @return string $resutn output of the command
 */
function setConnectionChannel($conn_name, $channel)
{
    $conn_name = escapeshellarg($conn_name);
    $channel = escapeshellarg($channel);

    exec("nmcli connection modify $conn_name 802-11-wireless.channel $channel",
            $result);

    return $result[0];
}

/**
 * Sets the band of the connection
 * 
 * @param string $conn_name name of the connection
 * @param string $band band
 * @param string $result output of the command
 */
function setConnectionBand($conn_name, $band)
{
    $conn_name = escapeshellarg($conn_name);
    $band = escapeshellarg($band);

    exec("nmcli connection modify $conn_name 802-11-wireless.band $band",
            $result);

    return $result[0];
}

/**
 * Get the SSID of the connection
 * 
 * @param string $conn_name connection name
 * @param string $ssid the ssid
 */
function getConnectionSSID($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli -t -f 802-11-wireless.ssid connection show $conn_name |
            awk -F':' '{print $2}'", $result);

    $ssid = $result[0];
    
    return $ssid;
}

/**
 * Returns an array in the form [setting => value] for the settings of the
 * connection. By default it returns interface, ssid, mode, band,
 * channel, security. User can pass an array with values the nmcli settings that wants to
 * be returned. If a setting is erroneous null is returned.
 * 
 * @param string $conn_name connection name
 * @param array $settings array with settings as values
 * @return array $settings the array with settings as keys and values as values
 */
function getConnectionSettings($conn_name, $settings = null)
{
    // Default settings
    if (!$settings) {
        $settings = [
            "connection.interface-name",
            "802-11-wireless.ssid",
            "802-11-wireless.band",
            "802-11-wireless.channel",
            "802-11-wireless.hidden",
            "802-11-wireless-security.key-mgmt",
            "802-11-wireless-security.pairwise",
            "802-11-wireless-security.psk"
        ];
    }

    $conn_name = escapeshellarg($conn_name);
    $settings_args = escapeshellarg(implode(",", $settings));

    exec("nmcli -t -f $settings_args connection show $conn_name --show-secrets|
            awk -F':' '{print $2}'", $result);

    // If error in command return null
    if (!$result) {
        return null;
    }

    $settings = array_combine(array_slice($settings, 0, count($result)),
                              $result);
    
    return $settings;
}

/**
 * Set multiple settings to the connection. The settings array is of the form
 * [setting => value]. If no error occurs returns null, else error string.
 * 
 * @param string $conn_name connection name
 * @param array $settings array with settings to be set
 * @return string $result command result
 */
function setConnectionSettings($conn_name, $settings)
{
    $imploded = $settings;
    array_walk($imploded, function (&$value, $key) {
        $value = "$key " . escapeshellarg($value);
    });

    $settings_args = implode(" ", $imploded);

    exec("nmcli connection modify $conn_name $settings_args 2>&1", $result);

    return $result ? $result[0] : null;
}

/**
 * Activates a connection. Returns string with D-Bus path or error.
 * 
 * @param string $conn_name connection name
 * @return string $result command result
 */
function activateConnection($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli connection up $conn_name 2>&1", $result);

    return $result[0];
}

/**
 * Deactivates a connection. Returns string with D-Bus path or error.
 * 
 * @param string $conn_name connection name
 * @return string $result null or string error
 */
function deactivateConnection($conn_name)
{
    $conn_name = escapeshellarg($conn_name);
    
    exec("nmcli connection down $conn_name 2>&1", $result);

    return $result[0];
}

/**
 * Restarts a connection. Returns array with nmcli messages.
 * 
 * @param string $conn_name connection name
 * @return array $result connection down & up command output
 */
function restartConnection($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli connection down $conn_name 2>&1 \
        && nmcli connection up $conn_name 2>&1",
        $result);

    return $result;
}

/**
 * Checks if connection is active. If connection is active return up else down
 * 
 * @param string $conn_name connection name
 * @param string $result "up"/"down"
 */
function checkConnectionIsActive($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli -t -f name connection show --active |
            awk '$1==\"$conn_name\" {print $1}'", $result);

    return $result[0] ? "up" : "down";
}

function setConnectionSecurityOpen($conn_name)
{
    $conn_name = escapeshellarg($conn_name);

    exec("nmcli connection modify $conn_name remove wifi-sec", $result);

    return $result;
}

/**
 * Starts the bridge master connection and modifies the active ethernet 
 * connection to become a slave to the master. Also modifies the connection
 * passed to be enslaved to the master connection also.
 * 
 * Password must be passed for wireless connections that have wifi-sec as 
 * password is removed when modifying the connection.
 * 
 * @param string $master_con_name connection name of the master bridge
 * @param string $master_ifname interface name of master bridge
 * @param string $active_eth_con_name connection name of the active ethernet connection
 * @param string $con_name connection name of the connection to become enslaved
 * @param string $key_mgmt
 * @param string $pairwise
 * @param string $psk the password of the connection to be enslaved if wifi connection with wifi-sec
 * @return string $result error message if some command failed, activation message if success
 */
function startEthernetBridge(
    $master_con_name,
    $master_ifname,
    $active_eth_con_name,
    $con_name,
    $key_mgmt,
    $pairwise,
    $psk
) {
    $master_con_name = escapeshellarg($master_con_name);
    $master_ifname = escapeshellarg($master_ifname);
    $active_eth_con_name = escapeshellarg($active_eth_con_name);
    $con_name = escapeshellarg($con_name);

    // Start master bridge
    exec("nmcli connection up $master_con_name 2>&1", $result);

    if (strpos($result[0], "Error:")) {
        return $result[0];
    }
    unset($result);

    // Enslave active ethernet connection
    exec("nmcli connection modify $active_eth_con_name \
        connection.master $master_ifname connection.slave-type bridge 2>&1",
        $result);
    
    if ($result[0]) {
        return $result[0];
    }

    // Restart active ethernet connection
    exec("nmcli connection up $active_eth_con_name 2>&1", $result);

    if (strpos($result[0], "Error:")) {
        return $result[0];
    }
    unset($result);

    // Modify connection to be enslave
    exec("nmcli connection modify $con_name connection.master $master_ifname \
        connection.slave-type bridge 2>&1", $result);
    if (strcmp($key_mgmt, "open")) {
        $settings["802-11-wireless-security.key-mgmt"] = $key_mgmt;
        $settings["802-11-wireless-security.pairwise"] = $pairwise;
        $settings["802-11-wireless-security.psk"] = $psk;
        setConnectionSettings($con_name, $settings);
    }

    if ($result[0]) {
        return $result[0];
    }
    unset($result);

    // Restart enslave connection
    exec("nmcli connection up $con_name 2>&1", $result);

    return $result[0];
}

/**
 * Stops the bridge master connection and modifies the active ethernet 
 * connection to become a simple connection with auto ipv4 & ipv6 addresses.
 * Also modifies the connection passed to be a wifi ap.
 * 
 * Password must be passed for wireless connections that have wifi-sec as 
 * password is removed when modifying the connection.
 * 
 * @param string $master_con_name connection name of the master bridge
 * @param string $master_ifname interface name of master bridge
 * @param string $active_eth_con_name connection name of the active ethernet connection
 * @param string $con_name connection name of the connection to become wifi ap
 * @param string $key_mgmt
 * @param string $psk
 * @param string $wpa_passphrase the password of the connection to be enslaved if wifi connection with wifi-sec
 * @return string $result error message if some command failed, activation message if success
 */
function stopEthernetBridge(
    $master_con_name,
    $master_ifname,
    $active_eth_con_name,
    $con_name,
    $key_mgmt,
    $pairwise,
    $psk
) {
    $master_con_name = escapeshellarg($master_con_name);
    $master_ifname = escapeshellarg($master_ifname);
    $active_eth_con_name = escapeshellarg($active_eth_con_name);
    $con_name = escapeshellarg($con_name);

    // Modify wifi ap connection
    exec("nmcli connection modify $con_name connection.master \"\" \
        connection.slave-type \"\" ipv4.method shared 2>&1", $result);
    if (strcmp($key_mgmt, "open")) {
        $settings["802-11-wireless-security.key-mgmt"] = $key_mgmt;
        $settings["802-11-wireless-security.pairwise"] = $pairwise;
        $settings["802-11-wireless-security.psk"] = $psk;
        setConnectionSettings($con_name, $settings);
    }

    if ($result) {
        return $result[0];
    }

    // Restart wifi ap
    exec("nmcli connection up $con_name", $result);
    
    if (strpos($result[0], "Error:")) {
        return $result[0];
    }
    unset($result);

    // Modify active ethernet connection
    exec("nmcli connection modify $active_eth_con_name connection.master \"\" \
        connection.slave-type \"\" ipv4.method auto ipv6.method auto 2>&1",
        $result);
    
    if ($result && strpos($result[0], "Error:")) {
        return $result[0];
    }
    unset($result);

    // Restart active ethernet connection
    exec("nmcli connection up $active_eth_con_name 2>&1", $result);

    if (strpos($result[0], "Error:")) {
        return $result[0];
    }
    unset($result);

    // Stop master bridge
    exec("nmcli connection down $master_con_name", $result);

    return $result[0];

}
