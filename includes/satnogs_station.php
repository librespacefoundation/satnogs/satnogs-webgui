<?php

require_once 'config.php';

session_start();
if (!$_SESSION['satnogs_network_url']) {
    $_SESSION['satnogs_network_url'] = RASPI_SATNOGS_NETWORK_URL;
}

/**
 * Generates the client id
 *
 */
function generateClientId()
{
  $json["satnogs_client_id"] = bin2hex(random_bytes(16));
  file_put_contents(RASPI_SATNOGS_CONFIG, json_encode($json, JSON_PRETTY_PRINT));
}


/**
 * Reads from config file or generates the client ID
 * 
 * @return string the client ID
 */
function getSatnogsClientId()
{
  $json = json_decode(file_get_contents(RASPI_SATNOGS_CONFIG), true);

  if(!$json["satnogs_client_id"]) {
    generateClientId();
  }

  return $json["satnogs_client_id"];
}

/**
 * Reads from config file the station ID. If the station ID is not saved to the
 * file a configuration request is send to the network and the config file is 
 * populated
 * 
 * @return int the station ID
 */
function getSatnogsStationId()
{
  $json = json_decode(file_get_contents(RASPI_SATNOGS_CONFIG), true);

  if(!$json["station_id"]) {
    $response = satnogsConfigurationRequest($json["satnogs_client_id"]);

    if(!$response) {
        return 0;
    }
    $json = json_decode($response, true);
    $net_conf_json = $json["Network Configuration"];
    setSatnogsStationId($net_conf_json["station_id"]);
    
    return $net_conf_json["station_id"];
  }

  return $json["station_id"];
}

/**
 * Writes station ID to config file.
 * 
 * @param int the station ID
 */
function setSatnogsStationId($stationId)
{
    $json = json_decode(file_get_contents(RASPI_SATNOGS_CONFIG), true);
    $json["station_id"] = $stationId;

    file_put_contents(RASPI_SATNOGS_CONFIG, json_encode($json, JSON_PRETTY_PRINT));
}

/**
 * Returns the stations edit link
 * 
 * @return string edit link
 */
function getSatnogsStationEditLink()
{
  $json = json_decode(file_get_contents(RASPI_SATNOGS_CONFIG), true);

  if(!$json["station_id"]) {
    $response = satnogsConfigurationRequest($json["satnogs_client_id"]);
    $json = json_decode($response, true);
  }

  return $_SESSION['satnogs_network_url'].'stations/edit/'.$json["station_id"].'/';
}

/**
 * Check if client id exists in network. If it exists set satnogs_registered
 * to true and return true else set and return false
 *
 * @return boolean the station is registered
 */
function getSatnogsStationRegistered()
{
  $json = json_decode(file_get_contents(RASPI_SATNOGS_CONFIG), true);
  $response = satnogsConfigurationRequest($json["satnogs_client_id"]);

  if($response) {
    setSatnogsStationRegistered(true);
    return true;
  } 
  setSatnogsStationRegistered(false);
  return false;
}

/**
 * Writes if station is registered to the config file.
 * 
 * @param boolean $satnogsStationRegistered if the station is registered
 */
function setSatnogsStationRegistered($satnogsStationRegistered)
{ 
  $json = json_decode(file_get_contents(RASPI_SATNOGS_CONFIG), true);
  $json["satnogs_station_registered"] = $satnogsStationRegistered;

  file_put_contents(RASPI_SATNOGS_CONFIG, json_encode($json, JSON_PRETTY_PRINT));
}

/**
 * Returns the latest 300 lines from journal for satnogs-client.service
 * 
 * @return array the journalctl output
 */
function getSatnogsJournal()
{
    exec('journalctl -u satnogs-client.service -n 300', $journal);
    return $journal;
}

/**
 * Calls a python script that uses satnogs-setup python classes to get support information for satnogs client
 * 
 * @return array the generated support information
 */
function getSatnogsSupport()
{
  $command = '/usr/bin/python3 /var/www/html/app/satnogs_support.py';
  exec($command, $support_info);

  return $support_info;
}

/**
 * Returns the SoapySDRUtil --probe output
 * 
 * @return array the SoapySDRUtil output
 */
function getSoapySDRUtil()
{
  exec('SoapySDRUtil --probe', $probe);

  return $probe;
}

/**
 * Creates and sends a post request to the Network API.
 * 
 * @param string $clientId the client ID
 * @return string the response of the server
 */
function satnogsRegistrationRequest($clientId)
{
  $postdata = http_build_query(
    array(
      'client_id' => $clientId
    )
  );
  $opts = array('http' => 
    array(
      'method' => 'POST',
      'content' => $postdata
    )
  );
  $context = stream_context_create($opts);
  $response = file_get_contents($_SESSION['satnogs_network_url'].'api/station/register', false, $context);

  return $response;
}

/**
 * Creates and sends a get request to the Network API for the station configuration.
 * 
 * @param string $clientId the client ID
 * @return string the response of the server
 */
function satnogsConfigurationRequest($clientId)
{
    $opts = array('http' =>
        array(
            'method' => 'GET',
            'header' => 'Authorization: '.$clientId
        )
    );
    $context = stream_context_create($opts);
    $response = file_get_contents($_SESSION['satnogs_network_url'].'api/configuration', false, $context);

    return $response;
}

/**
 * Creates and sends a get request to the Network API for the station information
 * 
 * @param int $stationId the station ID
 * @return string the response of the server
 */
function satnogsStationInformationRequest($stationId)
{
  $opts = array('http' =>
      array(
        'method' => 'GET',
        'Accept' => 'application/json'
      )
  );
  $context = stream_get_contents($opts);
  $response = file_get_contents($_SESSION['satnogs_network_url'].'api/stations/'.$stationId.'/');

  return $response;
}

/**
 * Returns a string of form Y years, M months ... ago till minutes.
 * User defines the number of units of time to be used starting from the most
 * significant.
 * If the difference is 0, minutes unit is used for the string.
 * 
 * @param DateInterval $interval the DateInterval object
 * @param int $max_units the number of different units to be added to the message
 * @return string the time message
 */
function printDateInterval ($interval, $max_units=2)
{
  $msg = "";
  $count = 0;

  if ($interval->y) {
    $msg .= $interval->y.' year(s) ';
    $count++;
  }
  if ($interval->m && $count < $max_units) {
      $msg .= $interval->m.' month(s) ';
      $count++;
  }
  if ($interval->d && $count < $max_units) {
    $msg .= $interval->d.' day(s) ';
    $count++;
  }
  if ($interval->h && $count < $max_units) {
    $msg .= $interval->h.' hour(s) ';
    $count++;
  }
  if ($interval->i && $count < $max_units) {
    $msg .= $interval->i.' minute(s) ';
  }

  if (!$msg) {
    return "0 minutes ago";
  }

  return $msg.' ago';

}

/**
 * Returns the frequency, as a string, formatted as xxx.xxx *Hz. * can be
 * nothing, K, M, G, T.
 * 
 * @param int $freq in Hz
 * @return string the formatted frequency
 */
function formattedAntennaFreq ($freq) 
{
  // Hz
  if ($freq < 1e3) {
    $tmp_freq = $freq;
    $suffix = 'Hz';
  }
  // KHz
  elseif ($freq < 1e6) {
    $tmp_freq = $freq / 1e3;
    $suffix = 'KHz';
  }
  // MHz
  elseif ($freq < 1e9) {
    $tmp_freq = $freq / 1e6;
    $suffix = 'MHz';
  }
  // GHz
  elseif ($freq < 1e12) {
    $tmp_freq = $freq / 1e9;
    $suffix = 'GHz';
  }
  else {
    $tmp_freq = $freq / 1e12;
    $suffix = 'THz';
  }

  return number_format($tmp_freq, 3) . " " . $suffix;
}

/**
 * Displays Satnogs Station tab
 */
function DisplayStation($clientId, $stationId, $stationRegistered)
{
    if (isset($_SESSION['status_messages'])) {
        $status = unserialize($_SESSION['status_messages']);
        unset($_SESSION['status_messages']);
    } else {
        $status = new \RaspAP\Messages\StatusMessage;
    }

    $satnogs_network_url = $_SESSION['satnogs_network_url'];
    $editLink = $_SESSION['satnogs_network_url'].'stations/edit/'.$stationId.'/';

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if (isset($_POST['info_network_url_input'])) {
            $_SESSION['satnogs_network_url'] = $_POST['info_network_url_input'];
        }
        $_SESSION['status_messages'] = serialize($status);
        header("Location: ". $_SERVER['REQUEST_URI'], true, 303);
        exit;
    }

    echo renderTemplate("satnogs_station", compact(
        'stationRegistered',
	'editLink',
	'satnogs_network_url'
    ));
}
