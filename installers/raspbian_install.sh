#!/bin/bash

#set -e

DEPENDENCIES=('lighttpd'\
           'git'\
           'php7.4-cgi'\
   	   'network-manager')

# Prints ASCII logo
function print_logo() {
    echo "

************************************************************
************************************************************
*                                                          *
*         _                                 _           _  *
* ___ ___| |_ ___ ___ ___ ___ ___ _ _ _ ___| |_ ___ _ _|_| *
*|_ -| .'|  _|   | . | . |_ -|___| | | | -_| . | . | | | | *
*|___|__,|_| |_|_|___|_  |___|   |_____|___|___|_  |___|_| *
*                    |___|                     |___|       *
*                                                          *
*             _         _       _ _                        *
*            |_|___ ___| |_ ___| | |___ ___                *
*            | |   |_ -|  _| .'| | | -_|  _|               *
*            |_|_|_|___|_| |__,|_|_|___|_|                 *
*                                                          *
************************************************************
************************************************************                                                

"
}
# Prints installer info
function print_info() {
    echo 'Usage:
    install  Installs satnogs-webgui and its dependencies
    
    remove   Removes satnogs-webgui and its dependencies
    
    update   Downloads and installs the latest satnogs-webgui
             version form the repo
    
    develop  Installs satnogs-webgui from the local repo
    
    info     Prints this message'
                 
}

# Prints dependencies
function print_dependencies() {
    echo 'satnogs-webgui dependencies:'
    MISSING_DEPENDENCIES=()
    for dep in ${DEPENDENCIES[@]}; do
        dep_installed=$(dpkg-query -W --showformat='${Status}\n' ${dep}\
                           2>/dev/null|grep "install ok installed")
        if [ "$dep_installed" = "" ]; then
            printf "\t${dep} [\033[0;31mMissing\033[0m]\n"
            MISSING_DEPENDENCIES+=("$dep")
        else
            printf "\t${dep} [\033[1;32mInstalled\033[0m]\n"
        fi
    done
    
    echo ''
}

# Downloads dependencies
function install_deps() {
    while true; do
        read -p 'Install missing dependencies? [yes/no] ' yn
        case $yn in
            [Yy] | [Yy]es) 
                apt update -qq
                for pkg in ${MISSING_DEPENDENCIES[@]}; do
                    printf "Installing $pkg...\n"
                    sudo apt-get -qq -o Dpkg::Use-Pty=0 install $pkg 
                    if [[ $? = 0 ]]; then
                        printf "$pkg [\033[1;32mSuccess\033[0m]\n"
                    else
                        printf "$pkg [\033[0;31mFail\033[0m]\n"
                    fi
                done
                break;;
            [Nn] | [Nn]o) printf 'Exiting\n'; exit 0;;
            * ) echo 'Answer yes or no';;
        esac
    done
}

# Enables fastcgi-php
function enable_fastcgi() {
    echo 'Enabling fastcgi'
    lighttpd-enable-mod fastcgi-php    
    service lighttpd force-reload
    systemctl restart lighttpd.service
    echo 'fastcgi enabled'

}

# Adds contents to serverroot
function install_content() {
    echo 'Installing content'
    rm -rf /var/www/html
    git clone https://gitlab.com/librespacefoundation/satnogs/satnogs-webgui /var/www/html
    cp /var/www/html/config/config.php /var/www/html/includes
    echo 'Installed content'
}

# Enable application routing
function enable_app_routing() {
    echo 'Enabling app routing' 
    WEBROOT="/var/www/html"
    CONFSRC="$WEBROOT/config/50-raspap-router.conf"
    LTROOT=$(grep "server.document-root" /etc/lighttpd/lighttpd.conf | awk -F '=' '{print $2}' | tr -d " \"")

    HTROOT=${WEBROOT/$LTROOT}
    HTROOT=$(echo "$HTROOT" | sed -e 's/\/$//')
    awk "{gsub(\"/REPLACE_ME\",\"$HTROOT\")}1" $CONFSRC > /tmp/50-raspap-router.conf
    cp /tmp/50-raspap-router.conf /etc/lighttpd/conf-available/

    ln -s /etc/lighttpd/conf-available/50-raspap-router.conf /etc/lighttpd/conf-enabled/50-raspap-router.conf
    systemctl restart lighttpd.service
    echo 'App routing enabled'
}

# Create needed dirs
function create_dirs() {
echo 'Creating directories'
mkdir /etc/raspap/
#mkdir /etc/raspap/backups
#mkdir /etc/raspap/networking
#mkdir /etc/raspap/hostapd
touch /etc/raspap/raspi_satnogs.conf

#cp raspap.php /etc/raspap
echo 'Directories created'
}

# Set ownership to www-data user
function set_ownership() {
echo 'Setting ownership'
chown -R www-data:www-data /var/www/html
chown -R www-data:www-data /etc/raspap
echo 'Ownership setted'
}

# Disables systemd-networkd
function disable_networkd() {
echo 'Disabling networkd'
systemctl stop systemd-networkd
systemctl disable systemd-networkd
echo 'Networkd disabled'
}

# Enables Network Manager
function enable_network_manager() {
echo 'Enabling Network Manager'
systemctl enable NetworkManager
systemctl start NetworkManager
echo 'Network Manager enabled'
}

# Copies the NM connections to correct path
function create_connections() {
echo 'Creating connections'
cp /var/www/html/config/system-connections/* /etc/NetworkManager/system-connections
chmod 600 /etc/NetworkManager/system-connections/*
chown root:root /etc/NetworkManager/system-connections/*
nmcli connection reload
echo 'Connections created'
}

# Changes NM permissions for www-data user
function change_nm_permissions() {
echo 'Changing NM permissions'
touch /etc/polkit-1/localauthority/50-local.d/50-NetworkManager.pkla
printf "[Network Manager www-data allow modify settings, enable/disable networking]\nIdentity=unix-user:www-data\nAction=org.freedesktop.NetworkManager.settings.modify.system;org.freedesktop.NetworkManager.network-control;org.freedesktop.NetworkManager.wifi.scan;org.freedesktop.NetworkManager.wifi.share.open;org.freedesktop.NetworkManager.wifi.share.protected;org.freedesktop.NetworkManager.enable-disable-statistics\nResultAny=yes" >> /etc/polkit-1/localauthority/50-local.d/50-NetworkManager.pkla

echo 'NM permissions changed'
}

function install() {
    echo 'Satnogs-webgui installer started'
    echo '************************************************************'
    echo 'Unblocking wlan'
    rfkill unblock wlan
    echo '************************************************************'
    echo 'Checking Dependencies...'
    # print dependencies
    print_dependencies
    # install dependencies
    install_deps
    # enable fastcgi
    enable_fastcgi
    # copy site contents
    install_content
    # enable app_routing
    enable_app_routing
    # create directories
    create_dirs
    # set ownership
    set_ownership
    # Disable networkd
    disable_networkd
    # enable Network Manager
    enable_network_manager
    # create NM connections
    create_connections
    # change NM permissions for user www-data
    change_nm_permissions
}

# Update satnogs-webgui
function update() {
    printf "Updating webgui from repository..."
    install_content
    printf "Update finished.\n"
}

# Install from local
function develop() {
    printf "Installing local webgui...\n"
    cd /home/pi/satnogs-webgui
    rm -rf /var/www/html/*
    cp -r * /var/www/html/
    cp config/config.php /var/www/html/includes
    chown -R www-data:www-data /var/www/html
    printf "Update finished.\n"

}

# Removes satnogs-webgui
function remove() {
    for pkg in ${DEPENDENCIES[@]}; do
        printf "Removing $pkg...\n"
        apt-get -qq -o Dpkg::Use-Pty=0 --purge remove $pkg
        if [[ $? = 0 ]]; then
            printf "$pkg [\033[1;32mSuccess\033[0m]\n"
        else
            printf "$pkg [\033[0;31mFail\033[0m]\n"
        fi
    done
    printf "Start autoremove...\n"
    apt autoremove

    printf "Remove folders and configurations...\n"
    rm -rf /etc/raspap/
    printf "Completed.\n"
}

function clean() {
    printf "Remove folders and configurations...\n"
    rm -rf /var/www/html/*
    printf "Completed.\n"
}

# Reads the script arguments and runs the specific function or prints info
# on erroneous arguments
function get_args() {
    if [[ "$1" = "" ]]; then
        echo "No arguments passed"
        print_info
        exit 1
    fi
    for arg in "$1"; do
        if [[ "$arg" = "install" ]]; then
            install
        elif [[ "$arg" = "remove" ]]; then
            remove
        elif [[ "$arg" = "info" ]]; then
            print_info
        elif [[ "$arg" = "update" ]]; then
            update
        elif [[ "$arg" = "develop" ]]; then
            develop
        elif [[ "$arg" = "clean" ]]; then
            clean
        else
            echo "Unknown argument"
            print_info
        fi
    done
}

print_logo
get_args $@
