/* GLOBAL VARIABLE TO SAVE CHART */
var traffic_rate_chart;

/**
 * Returns an array with the bytes passed in the best unit to represent them
 * with two decimal and the unit. If an index is passed the value is formatted
 * to the specified unit instead of the best
 * The values are calculated with 1024 base so the values are in *ibibytes.
 * 
 * E.g formatBytes(1300) => [1.27, 'KiB', 1]
 * @param {*} bytes the bytes to be formatted
 * @param {String} index specific unit for bytes to be formatted to or null
 * @returns {Array} array with formatted bytes, unit and unit index
 */
function formatBytes(bytes, index=null){  
  if (bytes == 0) {
    return [0, 'Bytes', 0];
  }

  const units = ['Bytes', 'KiB', 'MiB', 'GiB', 'TiB'];
  const base = 1024;
  const decimals = 2;

  index = units.indexOf(index);
  let i = index !=-1 ? index : Math.floor(Math.log(bytes) / Math.log(base));

  return [(bytes / Math.pow(base, i)).toFixed(decimals), units[i], i];
}

/**
 * Creates the Chart.js to plot network traffic speed and sets the options.
 * y ticks are formatted with a callback function depending the unit that is
 * saved to sessionStorage. Max number of ticks are 6. x axis ticks are hidden.
 * There are 20 ticks and they are RefreshRateMs apart as is setted in index.php
 * (default vaulue is 1000).
 * @param {String} ctx the canvas node id
 * @returns the chart
 */
function CreateChart(ctx) {
  let linechart = new Chart(ctx,{
    type: 'line',
    options: {
      responsive: true,
      scales: {
        xAxes: [{
          ticks: {
            display: false
          }
        }],
        yAxes: [{
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left',
          ticks: {
            callback: (val) => {
              const unit = sessionStorage.getItem("unit");
              const formatted_bytes = formatBytes(val, unit)[0];
              return (formatted_bytes + ' ' + unit);
            },
            beginAtZero: true,
            maxTicksLimit: 6
          }
        }]
      },
      tooltips: {
        enabled: false
      }
    },
    data: {
      labels: [...Array(10).keys()],
      datasets: [{
        label: 'TX',
        yAxisID: 'y-axis-1',
        borderColor: 'rgba(75, 192, 192, 1)',
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        data: Array(10).fill(0)
      },
      {
        label: 'RX',
        yAxisID: 'y-axis-1',
        borderColor: 'rgba(192, 192, 192, 1)',
        backgroundColor: 'rgba(192, 192, 192, 0.2)',
        data: Array(10).fill(0)
      }]
    }
  });
  return linechart;
}

/**
 * Adds a new value to the specified dataset of the chart. It pushes the new
 * value and shifts the array. Dataset with index 0 is the TX and with index 1
 * is RX.
 * @param {Object} chart the chart
 * @param {Number} value value to be added in bytes
 * @param {Number} index dataset index to add new data
 */
function addData(chart, value, index) {
  chart.data.datasets[index].data.push(value);
  chart.data.datasets[index].data.shift();
}

/**
 * Calls the chart update function. Uses an ajax call to get the rx and tx bytes
 * that are sent by the AP, saves them in the sessionStorage, calculates the
 * difference and calls addData to add them to the correct dataset. Also
 * calculates the unit that best format the max value in the datasets and also
 * saves them to sessionStorage so yaxes callback can format the datasets.
 * @param {Object} chart the chart
 * @see addData
 */
function updateChart(chart) {
  const uri = 'ajax/bandwidth/get_network_traffic.php';
  // Get number of tx/rx bytes untill now
  $.get(uri, function(data){
    const jsonData = JSON.parse(data);
    // Calculate traffic rate
    const tx_bytes_rate = (jsonData["txBytes"] -
                     parseInt(sessionStorage.getItem("tx_bytes")))
                     / 2;
    const rx_bytes_rate = (jsonData["rxBytes"] -
                     parseInt(sessionStorage.getItem("rx_bytes")))
                     / 2;

    // Update number of tx/rx bytes
    sessionStorage.setItem("tx_bytes", jsonData["txBytes"]);
    sessionStorage.setItem("rx_bytes", jsonData["rxBytes"]);

    // Update chart with new values
    addData(chart, tx_bytes_rate, 0);
    addData(chart, rx_bytes_rate, 1);

    // Find max number of bytes rate in both arrays
    const tx_array = chart.data.datasets[0].data;
    const rx_array = chart.data.datasets[1].data;
    let max_bytes = Math.max(...(tx_array.concat(rx_array)));

    const [bytes, unit, index] = formatBytes(max_bytes);
    sessionStorage.setItem("unit", unit);

    chart.update();
  });
}

/**
 * When DOM is ready, get with ajax request the values for TX/RX bytes and save
 * them to sessionStorage. Aslo set unit to Bytes in sessionStorage. Also create
 * chart and set the updateChart function to update every second.
 * @see updateChart
 */
$(document).ready(function() {
  sessionStorage.setItem("unit", "Bytes");

  // Get Tx/Rx bytes sent until now to calculate traffic speed
  let uri = 'ajax/bandwidth/get_network_traffic.php';
  $.get(uri, function(data){
    let jsonData = JSON.parse(data);
    sessionStorage.setItem("tx_bytes", jsonData["txBytes"]);
    sessionStorage.setItem("rx_bytes", jsonData["rxBytes"]);
  });

  traffic_rate_chart = CreateChart('divDBChartBandwidth');

});

/**
 * On button click change the fas icon accordingly. When the stop button is
 * pressed stop the update interval. When the start button is pressed destroy
 * the chart and create a new one
 */
$("#start_stop_traffic_chart").on("click", function(){
  // Play class
  if ($("#start_stop_traffic_chart_icon").hasClass("fas fa-play")) {
    traffic_rate_chart.destroy()
    traffic_rate_chart = CreateChart('divDBChartBandwidth');
    $("#start_stop_traffic_chart_icon").removeClass("fas fa-play");
    $("#start_stop_traffic_chart_icon").addClass("fas fa-stop");
    let start_stop_interval = setInterval(updateChart, 2000, traffic_rate_chart);
    sessionStorage.setItem("start_stop_interval", start_stop_interval);
  } else {
    $("#start_stop_traffic_chart_icon").removeClass("fas fa-stop");
    $("#start_stop_traffic_chart_icon").addClass("fas fa-play");
    clearInterval(sessionStorage.getItem("start_stop_interval"));
  }
});

