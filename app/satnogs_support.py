#!/usr/bin/env python3
import sys

# Get python version
major = sys.version_info.major
minor = sys.version_info.minor
python_ver = 'python' + str(major) + '.' + str(minor)

sys.path.insert(0, '/var/lib/satnogs-config/lib/' + python_ver + '/site-packages')
from satnogsconfig import config
import satnogsconfig.helpers as helpers
import satnogsconfig.settings as settings

satnogs_config = config.Config('/etc/ansible/host_vars/localhost')
satnogs_setup = helpers.SatnogsSetup()
ansible = helpers.Ansible(settings.ANSIBLE_DIR)

print(helpers.Support(satnogs_config, satnogs_setup, ansible).dump(indent=2))
