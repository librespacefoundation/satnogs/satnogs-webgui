<div class="card">
    <div class="card-body">
        <h5 class="card-title"><?php echo htmlspecialchars($network['ssid'], ENT_QUOTES); ?></h5>
        <!-- Status -->
        <div class="info-item-wifi"><?php echo _("Status"); ?></div>
        <div>
            <?php if (isset($network['configured'])): ?>
                <i class="fas fa-check-circle"></i>
            <?php endif; ?>
            <?php if (isset($network['active'])): ?>
                <i class="fas fa-exchange-alt"></i>
            <?php endif; ?>
            <?php if (!isset($network['configured']) && !$network['active']):
                echo _("Not configured");
            endif; ?>
        </div>
        <!-- Channel -->
        <div class="info-item-wifi"><?php echo _("Channel"); ?></div>
        <div>
            <?php if (isset($network['channel'])): ?>
                <?php echo htmlspecialchars($network['channel'], ENT_QUOTES) ?>
            <?php else: ?>
                <span class="label label-warning"> X </span>
            <?php endif; ?>
        </div>
        <!-- RSSI -->
        <div class="info-item-wifi"><?php echo _("RSSI"); ?></div>
        <div>
        <?php if(isset($network['rssi'])): ?>
            <?php echo htmlspecialchars($network['rssi'], ENT_QUOTES); ?>
            <?php echo "dB (".$network['signal']."%)" ?>
        <?php else: ?>
            <?php echo " Not Found " ?>
        <?php endif; ?>
        </div>
        <!-- Security -->
        <div class="info-item-wifi"><?php echo _("Security"); ?></div>
        <div>
            <?php echo $network['security'] ? $network['security'] : "-" ?>
        </div>
        <!-- Passphrase -->
        <div class="form-group">
            <div class="info-item-wifi"><?php echo _("Passphrase"); ?></div>
            <div class="input-group">
                <input type="password" class="form-control" aria-describedby="passphrase" name="passphrase_<?php echo $network["index"] ?>" value="<?php echo isset($network['password']) ? $network["password"] : "" ?>" data-target="#update<?php echo $network["index"] ?>" data-colors="#ffd0d0,#d0ffd0">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary js-toggle-password-multiple" type="button" id="passphrase_<?php echo $network["index"] ?>" data-target="[name=passphrase_<?php echo $network["index"] ?>]" data-toggle-with="fas fa-eye-slash"><i class="fas fa-eye mx-2"></i></button>
                </div>
            </div>
        </div>
        <!-- Connection buttons -->
        <div class="btn-group btn-block">
            <?php if (isset($network["configured"])):?>
		<button type="button" class="col-xs-4 col-md-4 btn btn-warning js-update-connection-nm" name="update_<?php echo $network["index"]?>" value="<?php echo $network["ssid"]?>"><?php echo _("Update"); ?></button>
                <?php if ($network["active"]):?>
                    <button type="button" class="col-xs-4 col-md-4 btn btn-info js-disconnect-nm" name="disconnect_<?php echo $network["index"]?>" value="<?php echo $network["ssid"]?>"><?php echo _("Disconnect"); ?></button>
                <?php else:?>
                    <button type="button" class="col-xs-4 col-md-4 btn btn-info js-connect-nm" name="connect_<?php echo $network["index"]?>" value="<?php echo $network["ssid"]?>"><?php echo _("Connect"); ?></button>
	        <?php endif;?>
            <?php else:?>
                <button type="button" class="col-xs-4 col-md-4 btn btn-info js-connect-nm" name="update_<?php echo $network["index"]?>" value="<?php echo $network["ssid"]?>"><?php echo _("Add"); ?></button>
            <?php endif;?>
            <button type="button" class="col-xs-4 col-md-4 btn btn-danger js-delete-connection-nm" name="delete_<?php echo $network["index"]?>" value="<?php echo $network["ssid"]?>" <?php echo (isset($network['configured']) ? '' : ' disabled')?>><?php echo _("Delete"); ?></button>
        </div>
    </div>
</div>
