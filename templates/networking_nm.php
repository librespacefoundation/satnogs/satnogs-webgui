<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <div class="card-header">
        <div class="row">
          <div class="col">
            <i class="fas fa-network-wired mr-2"></i><?php echo _("Networking"); ?>
          </div>
        </div>
      </div> <!-- card-header -->

      <div class="card-body">
        <ul class="nav nav-tabs">
          <li class="nav-item" role="presentation"><a class="nav-link active" href="#summary" aria-controls="summary" role="tab" data-toggle="tab"><?php echo _("Summary"); ?></a>
          </li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="summary">

            <!-- Internet connection -->
            <h4 class="mt-3"><?php echo _("Internet connection"); ?></h4>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th><?php echo _("Interface"); ?></th>
                        <th><?php echo _("IP Address"); ?></th>
                        <th><?php echo _("Gateway"); ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (!strcmp($connectivity, "none")): ?>
                        <tr><td colspan="5">No route to the Internet found</td></tr>
                      <?php else: ?>
                        <?php foreach ($interfaces_arr as $key => $iface): ?>
                        <tr>
                          <td><?php echo $key ?></td>
                          <td><?php foreach ($iface['ip_addresses'] as $ip): ?>
                          <?php echo $ip ?><br>
                          <?php endforeach ?> </td>
                          <td><?php foreach ($iface['gateway'] as $gateway): ?>
                          <?php echo $gateway ?><br>
                          <?php endforeach ?> </td>
                        </tr>
                        <?php endforeach ?>
                      <?php endif ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <!-- Routing Table -->
            <h4 class="mt-3"><?php echo _("Routing Table")?></h4>
            <ul class="nav nav-tabs" id="routes" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#ipv4-routes" aria-controls="basic" data-toggle="tab"><?php echo _("IPv4 Routes")?></a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" href="#ipv6-routes" data-toggle="tab"><?php echo _("IPv6 Routes")?></a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" role="tabpanel" id="ipv4-routes">
                <div class="card h-100 w-100">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="table-responsive">
                          <table class="table">
                            <tbody>
                              <tr>
                                <?php foreach ($interfaces_arr as $iface): ?>
                                  <?php foreach ($iface['ipv4_routes'] as $route): ?>
                                    <pre class="ustyled"><?php echo _($route)?></pre>
                                  <?php endforeach ?>
                                <?php endforeach ?>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!-- #ipv4-routes -->
              <div class="tab-pane" role="tabpanel" id="ipv6-routes">
                <div class="card h-100 w-100">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="table-responsive">
                          <table class="table">
                            <tbody>
                              <tr>
                                <?php foreach ($interfaces_arr as $iface): ?>
                                  <?php foreach ($iface['ipv6_routes'] as $route): ?>
                                    <pre class="ustyled"><?php echo _($route)?></pre>
                                  <?php endforeach ?>
                                <?php endforeach ?>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!-- #ipv6-routes -->
            </div> <!-- tab-content -->

            <h4 class="mt-3"><?php echo _("Current settings") ?></h4>
            <div class="row">
              <?php foreach ($interfaces_arr as $key => $iface): ?>
                <div class="col-sm mb-3">
                  <div class="card h-100 w-100">
                    <div class="card-header"><?php echo $key ?></div>
                    <div class="card-body">
                      <div class="row">
                        <div class="table-responsive">
                          <table class="table">
                            <pre class="unstyled" id="<?php echo $key ?>-summary"></pre>
                            <?php foreach ($iface['settings'] as $setting): ?>
                              <pre class="unstyled"><?php echo $setting ?></pre>
                            <?php endforeach ?>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach ?>
            </div><!-- /.row -->

            <button type="button" onClick="window.location.reload();" class="btn btn-outline btn-primary"><i class="fas fa-sync-alt"></i> <?php echo _("Refresh") ?></a>

          </div> <!-- tabpanel -->
        </div>
      </div>

      <div class="card-footer"><?php echo _("Information provided by Network Manager"); ?></div>

    </div>
  </div>
</div>
