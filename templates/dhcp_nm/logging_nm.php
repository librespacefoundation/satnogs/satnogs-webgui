<div class="tab-pane fade" id="logging_nm_dhcp">
  <h4><?php echo _("Logging") ?></h4>
  <div class="card h-100 w-100 mb-3">
    
    <div class="card-header">
      <?php echo _("Journalctl -b -u NetworkManager.service -n 300 | grep dhcp\|dns") ?>
      <button type="button" class="btn btn-card float-right ml-1 js-nm-dhcp-journal-table-copy" data-toggle="tooltip" data-placement="top" title="Copy"> <i class="fas fa-copy"></i></button>
      <button type="button" class="btn btn-card float-right ml-1 js-nm-dhcp-journal-table-refresh" data-toggle="tooltip" data-placement="top" title="Refresh"> <i class="fas fa-sync-alt"></i></button>
      <button type="button" class="btn btn-card float-right js-nm-dhcp-journal-table-export" data-toggle="tooltip" data-placement="top" title="Export"> <i class="fas fa-file-export"></i></button>
    </div>

    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive" id="nm_dhcp_journalctl_table">
            <table class="table">
              <tbody>
                Journal not generated. Refresh page or table to generate.
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div> <!-- card-body -->
  </div> <!-- card h-100 w-100 -->
  
  <div class="row mt-3">
    <div class="col-sm">
      <button type="button" class="btn btn-outline btn-primary js-dhcp-logs-refresh"> <i class="fas fa-sync-alt"></i> <?php echo _("Refresh") ?> </button>
      <button type="button" class="btn btn-outline btn-primary js-dhcp-logs-download"> <i class="fas fa-file-export"></i> <?php echo _("Export") ?> </button>
    </div>
  </div>

</div> <!-- tab-pane -->
