<div class="tab-pane active" id="server-settings-nm">
  <h4 class="mt-3">DHCP Server Settings</h4>
  
  <div class="row">
    <div class="form-group col-md-6">
      <label for="code">Connection</label>
        <?php SelectorOptions('connections', $connections, $hotspot, 'cbxdhcpconnectionnm', 'loadConnectionDHCPSelect');?>
    </div>
  </div>

  <h5 class="mt-1"><?php echo _("Connection IP Address Method"); ?></h5>
  <div class="row">
    <div class="form-group col-md-6">
      <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-light" id="labeldhcp-nm">
          <input type="radio" name="connection-ip-method" id="chkdhcp-nm" value="auto" autocomplete="off"> DHCP
        </label>
        <label class="btn btn-light" id="labelstatic-nm">
          <input type="radio" name="connection-ip-method" id="chkstatic-nm" value="manual" autocomplete="off"> Static IP
        </label>
        <label class="btn btn-light" id="labelshared-nm">
          <input type="radio" name="connection-ip-method" id="chkshared-nm" value="shared" autocomplete="off"> Shared
        </label>
      </div>
    </div>
  </div>

  <div class="mt-0 mb-0 ml-0 mr-0" id="staticoptions-nm" hidden>
    <h5 class="mt-1">Static IP options</h5>
    
    <div class="row" id="ipv4addrrow-nm">
      <div class="form-group col-md-6">
        <label for="code"><?php echo _("IP Address"); ?></label>
        <input type="text" class="form-control" id="txtipaddress-nm" name="StaticIP" placeholder="x.x.x.x/x">
      </div>
    </div>

    <div class="row" id="gatewayrow-nm">
      <div class="form-group col-md-6">
        <label for="code"><?php echo _("Default gateway"); ?></label>
        <input type="text" class="form-control" id="txtgateway-nm" name="DefaultGateway" placeholder="x.x.x.x">
      </div>
    </div>

  </div> <!-- #staticoptions-nm -->
  <?php echo $buttons ?>
</div> <!-- .tab-pane -->
