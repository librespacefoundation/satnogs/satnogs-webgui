<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col">
            <i class="fas fa-satellite-dish mr-2"></i><?php echo _("SatNOGS Station"); ?>
            <button class="btn btn-light btn-icon-split btn-sm service-status float-right" id="satnogs-network-status-button" data-toggle="tooltip" data-placement="bottom" title="Down">
              <span class="icon"><i class="fas fa-circle service-status-down" id="satnogs-network-status-indicator"></i></span>
              <span class="text service-status" id="satnogs-network-status-text">SatNOGS</span>
            </button>
          </div>
        </div><!-- ./row -->
      </div><!-- ./card-header -->
      <div class="card-body">

        <!-- Alert messages div -->
        <div id="satnogs_alert"></div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
          <li class="nav-item"><a class="nav-link active" id="infotab" href="#information" data-toggle="tab"><?php echo _("Information"); ?></a></li>
          <li class="nav-item"><a class="nav-link" id="configtab" href="#configuration" data-toggle="tab"><?php echo _("Configuration")?></a></li>
          <li class="nav-item"><a class="nav-link" id="logtab" href="#logging" data-toggle="tab"><?php echo _("Logs")?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <?php echo renderTemplate("satnogs_station/information", $__template_data) ?>
          <?php echo renderTemplate("satnogs_station/configuration", $__template_data) ?>
          <?php echo renderTemplate("satnogs_station/logging", $__template_data) ?>
        </div><!-- /.tab-content -->

      </div><!-- ./card-body -->
    </div><!-- /.card -->
  </div><!-- /.col-lg-12 -->
</div><!-- /.row -->
