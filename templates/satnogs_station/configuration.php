<div class="tab-pane fade" id="configuration">
  <div id="config"> <hr> </div>
  <div id="config_buttons">
    <div class="row">
      <div class="col-sm">
        <?php if (!$stationRegistered) : ?>
          <button type="button" class="btn btn-outline btn-primary js-satnogs-register" id="config_register_button"> <i class="fas fa-external-link-alt"></i> <?php echo _("Register") ?></button>
        <?php else : ?>
          <a href="<?php echo $editLink?>" target="_blank" class="btn btn-outline btn-primary">  <i class="fas fa-external-link-alt"></i> <?php echo _("Edit")?></a>
          <button class="btn btn-outline btn-primary js-satnogs-info-refresh"> <i class="fas fa-sync-alt"></i> <?php echo _("Refresh") ?>
        <?php endif ?>
      </div><!-- /.col-sm -->

      <div class="col-sm align-self-end">
        <form action="satnogs_station" method="POST">
        <?php echo CSRFTokenFieldTag() ?>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="url_span"><?php echo _("Network URL")?></span>
            </div>
	          <input type="url" class="form-control" name="info_network_url_input" id="info_network_url_input" aria-label="network-url" aria-describedby="url_span" value=<?php echo $satnogs_network_url?>>
	          <div class="input-group-append">
              <input class="btn btn-outline btn-primary js-info-network-url-update" type="submit" name="info_update_url" value="<?php echo _("Update URL")?>" >
            </div>
          </div><!-- input-group -->
        </form>
      </div><!-- col-sm end -->

    </div><!-- /.row -->
  </div>
</div><!-- /.tab-pane config -->

