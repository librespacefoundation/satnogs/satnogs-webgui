<?php

require_once '../../includes/nm_functions.php';

$json = json_decode(file_get_contents('php://input'), true);

$ifaceStatus = getInterfaceState($json["ifname"]);
$ifaceStatus = strcmp($ifaceStatus, "connected") ? "down" : "up";

echo $ifaceStatus;
