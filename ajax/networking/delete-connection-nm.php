<?php

require_once '../../includes/nm_functions.php';

$json = json_decode(file_get_contents('php://input'), true);

$result = deleteConnection($json["ssid"]);

if (strpos($result, "Error") !== false) {
    header("HTTP/1.0 400 Bad Request");
    echo $result;
}