<?php

require_once '../../includes/nm_functions.php';
session_start();

$json = json_decode(file_get_contents('php://input'), true);

$result = connectWifi($_SESSION['wifi_client_interface'],
                      $json["ssid"], $json["password"]);

if (strpos($result, "Error") !== false) {
    header("HTTP/1.0 400 Bad Request");
    echo $result;
}
