<?php

require '../../includes/nm_functions.php';
require '../../includes/config.php';

session_start();
$interface = $_SESSION['ap_interface'];

$bytes = array("txBytes" => getInterfaceTxBytes($interface),
                "rxBytes" => getInterfaceRxBytes($interface));

echo json_encode($bytes);