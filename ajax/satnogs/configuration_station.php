<?php

require_once '../../includes/satnogs_station.php';

$clientId = getSatnogsClientId();

$response = satnogsConfigurationRequest($clientId);

/* Return error if we didn't get a response */
if (!$response) {
  header("HTTP/1.0 404 Not Found");
  return;
}

$json = json_decode($response, true);
$conf_json = $json["Soapy Configuration"];

?>
<!-- Station Name -->
<h4 class="mt-3"><?php echo("Soapy Configuration")?></h4>

<div class="row">
  <div class="col-lg-6">
    <?php foreach ($conf_json as $key => $value) :?>
      <div class="row">
        <div class="col-sm-4">
          <span class="badge badge-secondary"><?php echo $key?></span>
        </div>
        <div class="col-sm">
          <span><?php echo ($value);?></span>
        </div>
    </div>
    <?php endforeach ?>
  </div><!-- /.col-lg-5 -->
</div><!-- /.row -->

<hr>