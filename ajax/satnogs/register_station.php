<?php 

require_once '../../includes/satnogs_station.php';

header('Content-type: application/json');

$clientId = getSatnogsClientId();

$response = satnogsRegistrationRequest($clientId);

echo ($response);
