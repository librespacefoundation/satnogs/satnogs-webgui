<?php

require_once '../../includes/satnogs_station.php';

$stationId = getSatnogsStationId();

$response = satnogsStationInformationRequest($stationId);
$json = json_decode($response, true);

/* Return error if we didn't get a response */
if (!$response) {
  header("HTTP/1.0 404 Not Found");
  return;
}

setSatnogsStationRegistered(true);

/* Satnogs information variables from json */
$station_msg = $json["id"].' - '.$json["name"];
$qthloc = $json["qthlocator"];
$lat = $json["lat"];
$lng = $json["lng"];
$alt = $json["altitude"];
$min_hor = $json["min_horizon"];

$antennas = $json["antenna"];
foreach ($antennas as &$ant) {
  $freq_min = $ant["frequency"];
  $freq_max = $ant["frequency_max"];
  $ant['tooltip_title'] = formattedAntennaFreq($freq_min).
                           " - ".
                           formattedAntennaFreq($freq_max);
}

$observations = $json["observations"];
$obs_link = $_SESSION['satnogs_network_url'].'observations/?station='.$stationId;

/* Status state */
$status = $json["status"];
if ($status === "Online") {
  $status_badge = 'badge-success';
}
elseif ($status === "Offline") {
  $status_badge = 'badge-danger';
}
elseif ($status === "Testing") {
  $status_badge = 'badge-warning';
}

/* Time messages */
$now = new DateTime("now", new DateTimeZone("Z"));

$created = new DateTime($json["created"]);
$created_interval = date_diff($now, $created);
$created_msg = printDateInterval($created_interval);

if ($json["last_seen"]) {
  $last_seen = new DateTime($json["last_seen"]);
  $last_seen_interval = date_diff($now, $last_seen);
  $last_seen_msg = printDateInterval($last_seen_interval);
  $last_seen_msg = "Last seen ".$last_seen_msg;
}
else {
  $last_seen_msg = "Never seen";
}

$client_version = $json["client_versions"];
$description = $json["description"];
$success_rate = $json["success_rate"];

/* Uptime message */
$uptime_cmd  = exec("uptime -p");
$uptime = substr($uptime_cmd, 3);

$tmp = strtok($uptime, ',');
$tmp .= ', '. strtok(',');
$uptime = $tmp.' ago';

?>

<!-- Station Name -->
<h4 class="mt-3"><?php echo($station_msg)?></h4>

<div class="row">
  <div class="col-lg-5">

    <!-- QTH Locator -->
    <?php if ($qthloc) :?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">QTH Locator</span>
      </div>
      <div class="col-sm">
        <span><?php echo ($qthloc);?></span>
      </div>
    </div>
    <?php endif ?>
    
    <!-- Coordinates -->
    <?php if ($lat || $lng) :?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Coordinates(lat, lon)</span>
      </div>
      <div class="col-sm">
        <span><?php echo ("${lat}°, ${lng}°");?></span>
      </div>
    </div>
    <?php endif ?>
    
    <!-- Altitude -->
    <?php if ($alt) :?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Altitude</span>
      </div>
      <div class="col-sm">
        <span><?php echo ("${alt} m");?></span>
      </div>
    </div>
    <?php endif ?>
          
    <!-- Horizon -->
    <?php if ($min_hor) :?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Min Horizon</span>
      </div>
      <div class="col-sm">
        <span><?php echo ("${min_hor}°");?></span>
      </div>
    </div>
    <?php endif ?>
          
    <!-- Antennas -->
    <?php if ($antennas) :?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Antennas</span>
      </div>
      <div class="col-sm">
        <?php foreach ($antennas as $key => $value) :?>
        <span class="antenna-pill" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value["tooltip_title"]?>"><?php echo "${value['antenna_type_name']} (${value['band']})"?></span>
        <?php endforeach ?>
      </div>
    </div>
    <?php endif ?>

    <!-- Succes Bar -->
    <?php if ($observations && $success_rate) : ?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Success Rate</span>
      </div>
      <div class="col-sm-5">
        <div class="progress">
          <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo ($success_rate)?>%" aria-valuenow="<?php echo ($success_rate)?>" aria-valuemax=100
               data-toggle="tooltip" data-placement="bottom" title="<?php echo($success_rate.' %')?>"></div>
          <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo (100 - $success_rate)?>%" aria-valuenow="<?php echo (100 - $success_rate)?>" aria-valuemax=100
               data-toggle="tooltip" data_placement="bottom" title="<?php echo($success_rate.' %')?>"></div>
        </div>
      </div>
    </div>
    <?php endif ?>

    <!-- Observations -->
    <?php if ($observations) : ?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Observations</span>
      </div>
      <div class="col-sm">
        <span><?php echo ($observations)?></span>
      </div>
      <div class="col-sm">
        <a href="<?php echo ($obs_link)?>" target="_blank" class="badge badge-info">View all</a>
      </div>
    </div>
    <?php endif ?>
          
    <!-- Creation Date -->
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Creation Date</span>
      </div>
      <div class="col-sm">
        <span data-toggle="tooltip" data-placement="bottom" title=<?php echo ($json["created"])?>><?php echo $created_msg?></span>
      </div>
    </div>

    <!-- Client Version -->
    <?php if ($client_version) :?>
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Client Version</span>
      </div>
      <div class="col-sm">
        <span><?php echo $client_version?></span>
      </div>
    </div>
    <?php endif ?>
          
    <!-- Status -->
    <div class="row">
      <div class="col-sm-4">
        <span class="badge <?php echo ($status_badge)?>"><?php echo ($json["status"])?></span>
      </div>
      <div class="col-sm">
        <span data-toggle="tooltip" title=""><?php echo ($last_seen_msg)?></span>
      </div>
    </div>
    
    <!-- Uptime -->
    <div class="row">
      <div class="col-sm-4">
        <span class="badge badge-secondary">Uptime</span>
      </div>
      <div class="col-sm">
        <span><?php echo ($uptime) ?></span>
      </div>
    </div>

  </div><!-- /.col-md-4 -->
</div><!-- /.row -->

<!-- Station description -->
<?php if ($description) :?>
<hr>
<?php echo ($description);?>
<?php endif?>
<hr>
